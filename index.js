var http 					= require("http");
var querystring 	= require("querystring");
var EventEmitter2 = require('eventemitter2').EventEmitter2;
var util					= require('util');
var _							= require('lodash');

require('consoleplusplus');

var BWL	=	function(pubsub, filters){ 
	EventEmitter2.call( this );

	this.filters = filters || {};
	console.debug("Loaded filters: ", filters);

	var bwl = this;
	pubsub.on('payload', function(payload){
		console.info('New request.');

		if( ! bwl.matchfilters( payload ) ){return;}

		if( ! _.size(payload.commits)){
			return console.info("No commits.");
		}

		_.forEach(payload.commits, function( commit ){
			this.emit(['branch', commit.branch], commit);
			this.emit(['author', commit.author], commit);
		}, bwl);
	});
}

util.inherits(BWL, EventEmitter2);

BWL.prototype.matchfilters = function( payload ) {

	if(!_.size(this.filters)){
		console.debug('No filters [#green{Pass}]');
		return true;
	}

	var pass = true;

	if(_.has(this.filters, 'repo') && _.isNull( payload.repository.absolute_url.match(this.filters.repo) )){
		console.debug('Repo does not match. [#orange{SKIP}]');
		pass = false;
	}

	if(pass){console.debug('Filters [#green{Pass}]');};
	return pass;
};

var listen = function listen( port ){

	if( ! port ){
		throw new Error('.listen() needs port in first argument ( .listen( port ) )');
	}

	var pubsub = new EventEmitter2();

	this.server = http.createServer( function( req, res ) {
		

		// fetchin' data only from POST reqs
		if ( req.method !== "POST" ) {
			res.statusCode = 404;
			return res.end('404');			
		}

		var data = "";
		req.on( "data", function( chunk ) {
			data += chunk;
		});

		req.on( "end", function() {
			// expeted data should start with ^payload..
			if ( ! /^payload=/.test( data ) ) {
				res.statusCode = 404;
				util.error(' expected data should start with "payload="');
				return res.end('404');				
			}

			console.info("Request from: ", req.connection.remoteAddress);

			var payload = JSON.parse( querystring.unescape( data.slice(8), '+' ) );

			pubsub.emit('payload', payload);

			res.writeHead( 200, {
					'Content-type': 'text/html'
				});

			res.end();

		});
	}).listen( port );
	
	return function(filters){
		var filters = filters || {};

		return new BWL(pubsub, filters);
	}
}

module.exports = listen;
module.exports.listen = listen;
